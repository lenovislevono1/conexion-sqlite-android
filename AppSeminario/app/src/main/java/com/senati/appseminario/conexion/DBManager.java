package com.senati.appseminario.conexion;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.senati.appseminario.model.Asistente;
import com.senati.appseminario.util.AsistenteUtil;
import com.senati.appseminario.util.Constantes;

public class DBManager {
    public static final String TAG="DBManager";

    DBHelper helper = null;
    SQLiteDatabase db = null;

    public DBManager(Context context) {
        helper = new DBHelper(context);
        db = helper.getWritableDatabase(); //Se crea la Base de datos para escritura
    }

    public long insertarAsistente(Asistente asistente){
        Log.d(TAG,">>>Metodo insertarAsistente()");
        long result = db.insert(Constantes.TABLE_NAME,null, AsistenteUtil.getValoresAsistente(asistente));

        if(result != -1){
            Log.d(TAG,">>>Se ha insertado correctamente");
        }else{
            Log.d(TAG,">>>Error de inserción");
        }

        return result;
    }

    public void eliminarAsistente(int idAsis){
        Log.d(TAG,">>>Metodo eliminarAsistente()");
    }

    public void listarAsistente(){
        Log.d(TAG,">>>Metodo listarAsistente()");
    }

    public void actualizarAsistente(Asistente asistente){
        Log.d(TAG,">>>Metodo actualizarAsistente()");
    }


}
